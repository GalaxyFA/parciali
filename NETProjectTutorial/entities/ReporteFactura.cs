﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.entities
{
    class ReporteFactura
    {
        //factura
        private string cod_factura;
        private DateTime fecha;
        private Empleado empleado;
        private string observaciones;
        private double subtotal;
        private double iva;
        private double total;

        //DetalleFactura
        private int id_factura;
        private int id_producto;
        private int cantidad;
        private double precio;

        //producto
        private string sku;
        private string nombre;

        public ReporteFactura(string cod_factura, DateTime fecha, Empleado empleado, string observaciones, double subtotal, double iva, double total, int id_factura, int id_producto, int cantidad, double precio, string sku, string nombre)
        {
            this.cod_factura = cod_factura;
            this.fecha = fecha;
            this.empleado = empleado;
            this.observaciones = observaciones;
            this.subtotal = subtotal;
            this.iva = iva;
            this.total = total;
            this.id_factura = id_factura;
            this.id_producto = id_producto;
            this.cantidad = cantidad;
            this.precio = precio;
            this.sku = sku;
            this.nombre = nombre;
        }

        public string Cod_factura
        {
            get
            {
                return cod_factura;
            }

            set
            {
                cod_factura = value;
            }
        }

        public DateTime Fecha
        {
            get
            {
                return fecha;
            }

            set
            {
                fecha = value;
            }
        }

        internal Empleado Empleado
        {
            get
            {
                return empleado;
            }

            set
            {
                empleado = value;
            }
        }

        public string Observaciones
        {
            get
            {
                return observaciones;
            }

            set
            {
                observaciones = value;
            }
        }

        public double Subtotal
        {
            get
            {
                return subtotal;
            }

            set
            {
                subtotal = value;
            }
        }

        public double Iva
        {
            get
            {
                return iva;
            }

            set
            {
                iva = value;
            }
        }

        public double Total
        {
            get
            {
                return total;
            }

            set
            {
                total = value;
            }
        }

        public int Id_factura
        {
            get
            {
                return id_factura;
            }

            set
            {
                id_factura = value;
            }
        }

        public int Id_producto
        {
            get
            {
                return id_producto;
            }

            set
            {
                id_producto = value;
            }
        }

        public int Cantidad
        {
            get
            {
                return cantidad;
            }

            set
            {
                cantidad = value;
            }
        }

        public double Precio
        {
            get
            {
                return precio;
            }

            set
            {
                precio = value;
            }
        }

        public string Sku
        {
            get
            {
                return sku;
            }

            set
            {
                sku = value;
            }
        }

        public string Nombre
        {
            get
            {
                return nombre;
            }

            set
            {
                nombre = value;
            }
        }
    }
}
