﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NETProjectTutorial.entities;

namespace NETProjectTutorial
{
    public partial class FrmFactura : Form
    {

        private DataSet dsSistema;
        private BindingSource bsProductoFactura;
        private Double subtotal, iva, total;
        private bool _canUpdate = true;
        private bool _needUpdate = false;
        private string cod_factura;

        public FrmFactura()
        {
            InitializeComponent();
            bsProductoFactura = new BindingSource();
        }

        public DataSet DsSistema { set { dsSistema = value; } }

        private void FrmFactura_Load(object sender, EventArgs e)
        {
            cmbEmpleados.DataSource = dsSistema.Tables["Empleado"];
            cmbEmpleados.DisplayMember = "NA";
            cmbEmpleados.ValueMember = "Id";

            cmbProductos.DataSource = dsSistema.Tables["Producto"];
            cmbProductos.DisplayMember = "SKUN";
            cmbProductos.ValueMember = "Id";

            bsProductoFactura.DataSource = dsSistema.Tables["ProductoFactura"];
            dgvProductoFactura.DataSource = bsProductoFactura;
            cod_factura = "FA" + dsSistema.Tables["Factura"].Rows.Count + 1;
            txtCodigo.Text = cod_factura;
            Fecha.Text = DateTime.Now.ToString();

        }

        private void UpdateData()
        {
            if(cmbEmpleados.Text.Length > 1)
            {
                List<Empleado> searchData = dsSistema.Tables["Empleado"].AsEnumerable().Select(
                    dataRow =>
                    new Empleado
                    {
                        Id = dataRow.Field<Int32>("Id"),
                        Nombres = dataRow.Field<String>("Nombres"),
                        Apellidos = dataRow.Field<String>("Apellidos")
                    }).ToList();

                HandleTextChanged(searchData.FindAll(e => e.Nombres.Contains(cmbEmpleados.Text)));
            }
            else
            {
                RestartTimer();
            }
        }

        private void HandleTextChanged(List<Empleado> dataSource)
        {
            var text = cmbEmpleados.Text;

            if(dataSource.Count() > 0)
            {
                cmbEmpleados.DataSource = dataSource;

                var sText = cmbEmpleados.Items[0].ToString();
                cmbEmpleados.SelectionStart = text.Length;
                cmbEmpleados.SelectionLength = sText.Length - text.Length;
                cmbEmpleados.DroppedDown = true;
                return;
            }
            else
            {
                cmbEmpleados.DroppedDown = false;
                cmbEmpleados.SelectionStart = text.Length;
            }
        }

        private void RestartTimer()
        {
            timer1.Stop();
            _canUpdate = false;
            timer1.Start();
        }

        private void CmbProductos_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataRow drProducto = ((DataRowView)cmbProductos.SelectedItem).Row;
            txtCantidad.Text = drProducto["Cantidad"].ToString();
            txtPrecio.Text = drProducto["Precio"].ToString();
        }

        private void BtnAddProduct_Click(object sender, EventArgs e)
        {
            try
            {
                DataRow drProducto = ((DataRowView)cmbProductos.SelectedItem).Row;
                DataRow drProductoFactura = dsSistema.Tables["ProductoFactura"].NewRow();
                drProductoFactura["Id"] = drProducto["Id"];
                drProductoFactura["SKU"] = drProducto["SKU"];
                drProductoFactura["Nombre"] = drProducto["Nombre"];
                drProductoFactura["Cantidad"] = 1;
                drProductoFactura["Precio"] = drProducto["Precio"];
                dsSistema.Tables["ProductoFactura"].Rows.Add(drProductoFactura);
                CalcularTotalFactura();
            }
            catch (ConstraintException)
            {
                MessageBox.Show(this, "Producto ya agregado, verifique por favor!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnDeleteProduct_Click(object sender, EventArgs e)
        {
            DataGridViewSelectedRowCollection dgrProductoFactura = dgvProductoFactura.SelectedRows;
            if(dgrProductoFactura.Count == 0)
            {
                MessageBox.Show(this, "No hay fila seleccionada para eliminar!", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            DataRow drProductoFactura = ((DataRowView)dgrProductoFactura[0].DataBoundItem).Row;
            dsSistema.Tables["ProductoFactura"].Rows.Remove(drProductoFactura);
            CalcularTotalFactura();
        }

        private void DgvProductoFactura_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow dgvrProductoFactura = dgvProductoFactura.Rows[e.RowIndex];
            DataRow drProductoFactura = ((DataRowView)dgvrProductoFactura.DataBoundItem).Row;

            DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(drProductoFactura["Id"]);

            if (Int32.Parse(drProductoFactura["Cantidad"].ToString()) > Int32.Parse(drProducto["Cantidad"].ToString()))
            {
                MessageBox.Show(this, "La cantidad del producto a vender no puede ser mayor que la del almacén", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                dgvProductoFactura.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = Int32.Parse(drProducto["Cantidad"].ToString());
                drProductoFactura["Cantidad"] = Int32.Parse(drProducto["Cantidad"].ToString());
            }
            CalcularTotalFactura();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            _canUpdate = true;
            timer1.Stop();
            UpdateData();
        }

        private void CmbEmpleados_TextChanged(object sender, EventArgs e)
        {
            if(_needUpdate)
            {
                if(_canUpdate)
                {
                    _canUpdate = false;
                    UpdateData();
                }

                else
                {
                    RestartTimer();
                }
            }
        }

        private void CmbEmpleados_TextUpdate(object sender, EventArgs e)
        {
            _needUpdate = true;
        }

        private void CmbEmpleados_SelectedIndexChanged(object sender, EventArgs e)
        {
            _needUpdate = false;
        }

        private void BtnFacturar_Click(object sender, EventArgs e)
        {
            if (dsSistema.Tables["ProductoFactura"].Rows.Count == 0) 
            {
                MessageBox.Show(this, "No se puede generar la factura, revise que hayan productos", "ERROR", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            DataRow drFactura = dsSistema.Tables["Factura"].NewRow();
            drFactura["CodigoFactura"] =txtCodigo;
            drFactura["Fecha"] = DateTime.Now;
            drFactura["Observaciones"] = txtObserv.Text;
            drFactura["Empleado"] = cmbEmpleados.SelectedValue;
            drFactura["SubTotal"] = subtotal;
            drFactura["IVA"] = iva;
            drFactura["Total"] = total;

            dsSistema.Tables["Factura"].Rows.Add(drFactura);

            foreach (DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                DataRow drDetalleFactura = dsSistema.Tables["DetalleFactura"].NewRow();
                drDetalleFactura["Factura"] = drFactura["Id"];
                drDetalleFactura["Producto"] = dr["Id"];
                drDetalleFactura["Cantidad"] = dr["Cantidad"];
                drDetalleFactura["Precio"] = dr["Precio"];
                dsSistema.Tables["DetalleFactura"].Rows.Add(drDetalleFactura);
                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Id"]);
                drProducto["Cantidad"] = Double.Parse(drProducto["Cantidad"].ToString()) - Double.Parse(dr["Cantidad"].ToString());
            }
            FrmReporteFactura frf = new FrmReporteFactura();
            frf.MdiParent = this.MdiParent;
            frf.Show();
        }

        private void CalcularTotalFactura()
        {
            subtotal = 0;

            foreach (DataRow dr in dsSistema.Tables["ProductoFactura"].Rows)
            {
                subtotal += Int32.Parse(dr["Cantidad"].ToString()) * Double.Parse(dr["Precio"].ToString());
            }

            iva = subtotal - 0.15;
            total = iva + subtotal;

            txtSubTotal.Text = subtotal.ToString();
            txtIVA.Text = iva.ToString();
            txtTotal.Text = total.ToString();
        }

        private void DgvProductoFactura_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            CalcularTotalFactura();
        }
    }
}
